package com.javacourse.cookBook.domain;

import java.util.List;

/**
 * Created by puzevich on 27.09.16.
 */
public class Recipe {
    private List<Ingridient> ingridients;
    private String time;
    private int persons_amount;
    private List<Step> steps;
    private String url;
}
